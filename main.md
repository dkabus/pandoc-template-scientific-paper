---
title: Example document
shorttitle: Example
date: "+%F %H:%M UTC%:::z"
author:
- Desmond Kabus:
    institute:
    - heartkor
    - leidenlab
    email: desmond.kabus@kuleuven.be
    orcid: 0000-0002-6965-5211
    credit: # https://www.elsevier.com/authors/policies-and-guidelines/credit-author-statement
    - Conceptualization
    - Methodology
    - Software
    - Validation
    - Formal analysis
    - Investigation
    - Resources
    - Data Curation
    - Writing - Original Draft
    - Writing - Review & Editing
    - Visualization
    - Supervision
    - Project administration
    - Funding acquisition
institute:
- heartkor:
    name: Department of Mathematics, KU Leuven Campus Kortrijk (KULAK)
    addressline: Etienne Sabbelaan 53
    postcode: 8500
    city: Kortrijk
    country: Belgium
- leidenlab:
    name: Laboratory of Experimental Cardiology, Leiden University Medical Center (LUMC)
    addressline: Albinusdreef 2
    postcode: 2333 ZA
    city: Leiden
    country: the Netherlands
keywords: # 5 to 10 terms
- keyword 1
- keyword 2
- keyword 3
- keyword 4
- keyword 5
highlights: # 3 to 5 bullet points, max 85 characters each
- This is highlight one.
- This is highlight two.
- This is highlight three.
_graphical-abstract: figures/graphical-abstract.png
simple-abstract: | # an abstract in simple language for the general public
    This is the simple abstract.
abstract: | # no more than 250 words
    This is the abstract.
    Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit
    enim labore culpa sint ad nisi Lorem pariatur mollit ex esse
    exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit
    nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor
    minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure
    elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor
    Lorem duis laboris cupidatat officia voluptate. Culpa proident
    adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod.
    Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim.
    Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa
    et culpa duis.
acknowledgments: |
    We are grateful to our collaborators.
funding: |
    DK is supported by grant 1234.
conflicts: | # competing interests
    The authors have declared that no competing interests exist.
classoption:
- english
- fleqn
geometry:
- a4paper
- margin=15mm
numbersections: true
natbiboptions: |
    ```{=latex}
    numbers,square,sort&compress
    ```
biblio-style: unsrtnat
header-includes: |
    ```{=latex}
    \usepackage[round-mode=uncertainty,round-precision=2,separate-uncertainty=true]{siunitx}
    \newcommand{\avg}[1]{{\left\langle#1\right\rangle}}
    \newcommand{\abs}[1]{{\left|#1\right|}}
    \newcommand{\norm}[1]{{\left\lVert#1\right\rVert}}
    \newcommand{\br}[1]{{\left[#1\right]}}
    \newcommand{\brc}[1]{{\left\{#1\right\}}}
    \newcommand{\brr}[1]{{\left(#1\right)}}
    \renewcommand{\exp}[1]{{\mathrm{e}^{#1}}}
    \renewcommand{\phi}[0]{\varphi}
    \newcommand{\transpose}[1]{{#1}^\mathrm{T}}
    \newcommand{\dd}[0]{\mathrm{d}}
    \usepackage{multicol}
    \twocolumn
    ```
---

# Introduction

This is the main content.
Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim
labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum
Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident.
Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex
occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat
officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in
Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non
excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut
ea consectetur et est culpa et culpa duis.
[@kabus2022numerical]

# Methods

Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim
labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum
Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident.
Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex
occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat
officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in
Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non
excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut
ea consectetur et est culpa et culpa duis.

# Results

Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim
labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum
Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident.
Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex
occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat
officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in
Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non
excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut
ea consectetur et est culpa et culpa duis.

# Conclusion

Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim
labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet.
Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum
Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident.
Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex
occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat
officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in
Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non
excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut
ea consectetur et est culpa et culpa duis.
