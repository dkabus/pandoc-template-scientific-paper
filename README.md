# Pandoc template for a scientific paper

Markdown/TeX source code for a scientific paper.

## Requirements to compile

You may need to first download all submodules included in this repository:
```{=sh}
$ make submodules
```

Just compile with `make`:
```{=sh}
$ make
```

You need these tools installed:

- [GNU Make](https://www.gnu.org/software/make/)
- [Pandoc](https://pandoc.org/)
- a full [LaTeX installation](https://wiki.archlinux.org/title/TeX_Live) with:
    - `latexmk`
    - `pdflatex`
    - `biblatex`
    - `natbib`

## Files

- main document:
    - [`main.md`](main.md): Markdown source file. Edit this file!
    - [`main.tex`](main.tex): LaTeX code from compiling the Markdown file via Pandoc.
    - [`main.pdf`](main.pdf): Resulting PDF file from compiling the LaTeX file.
- tracked changes:
    - [`diff.tex`](diff.tex): LaTeX code from tracking changes with `latexdiff`
    - [`diff.pdf`](diff.pdf): Resulting PDF file from compiling the LaTeX file.
- letter to the editor:
    - [`letter.md`](letter.md): Markdown source file. Edit this file!
    - [`letter.tex`](letter.tex): LaTeX code from compiling the Markdown file via Pandoc.
    - [`letter.pdf`](letter.pdf): Resulting PDF file from compiling the LaTeX file.
- response to reviewers:
    - [`response.md`](response.md): Markdown source file. Edit this file!
    - [`response.tex`](response.tex): LaTeX code from compiling the Markdown file via Pandoc.
    - [`response.pdf`](response.pdf): Resulting PDF file from compiling the LaTeX file.

## Compilation via Gitlab CI

This repository contains configuration to automatically compile the document
and publish it via Gitlab Pages. You can find the pages here:

<https://dkabus.gitlab.io/pandoc-template-scientific-paper/>

<https://gitlab.com/dkabus/pandoc-template-scientific-paper/>
