---
title: Response to Reviewers
author: Desmond Kabus et al.
date: "+%F %H:%M UTC%:::z"
lang: en
fontsize: 11pt
geometry:
- a4paper
- margin=28mm
numbersections: 2
natbiboptions: |
    ```{=latex}
    numbers,square,sort&compress
    ```
biblio-style: unsrtnat
header-includes: |
  ```{=latex}
  \usepackage{framed}
  \usepackage{xcolor}
  \let\oldquote=\quote
  \let\endoldquote=\endquote
  \colorlet{shadecolor}{orange!15}
  \renewenvironment{quote}{\vspace{5mm}\begin{shaded*}\begin{oldquote}}{\end{oldquote}\end{shaded*}}
  \newcommand{\TODO}[0]{\textcolor{purple}{TODO}}
  \newcommand{\TODISCUSS}[0]{\textcolor{blue}{TO DISCUSS}}
  ```
---

> Comment \TODISCUSS

\TODO: write response to reviewers
