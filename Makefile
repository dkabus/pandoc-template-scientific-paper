# define targets
all: main diff letter response
main: main.pdf main.tex
letter: letter.pdf letter.tex
response: response.pdf response.tex
diff: diff.pdf diff.tex

# variables
luafilters=$(wildcard filters/*.lua)
sedfilters=$(wildcard filters/*.sed)
auxdir=/tmp/latexmk/$(subst /,%,$(PWD))
version=$(shell git show -s --format=%ci | xargs -I"{}" date -d "{}" +"%Y-%m-%d")-$(shell git rev-parse --short HEAD)$(shell git diff --quiet || echo '-dev')
oldhash=$(shell echo $(oldtex) | sed 's/-[^-]\+\.tex$$//;s/versions\///;s/^[0-9-]\{10\}-//')
oldtex=$(shell (ls versions/*.tex || echo) | sort -r | sed '/\<response\>/d;/\<diff\>/d;/\<letter\>/d' | sed 1q)

main.tex: main.md main.bib $(luafilters) $(sedfilters)
	# markdown to tex for main file
	pandoc -s \
		$(addprefix --lua-filter=,$(luafilters)) \
		--natbib --bibliography=$(word 2,$^) \
		-o $@ $<
	echo $(sedfilters) | xargs -rI {} sed -f {} -i $@

%.tex: %.md main.bib
	# markdown to tex for other files
	pandoc -s -o $@ $< \
		--lua-filter=filters/available/date-git-commit/date.lua \
		# --natbib --bibliography=$(word 2,$^)

diff.tex: $(oldtex) main.tex
	# track changes
	# option to always mark up whole equations --math-markup=whole
	test $(words $^) -eq 2 && latexdiff $^ > $@ || cp -v $< $@
	sed -i '/LATEXDIFF DIFFERENCE FILE/,+2d;s/\\documentclass\[/&draft,/' $@

%.pdf: %.tex
	# compile latex
	mkdir -p -m700 '$(auxdir)'
	latexmk -quiet -aux-directory='$(auxdir)' -pdflua -interaction=nonstopmode -f -time $<

version:
	# print version
	@echo $(version)

copy-to-versions: all
	# copy current file to subdirectory "versions" to track changes
	@mkdir -p versions/
	@test ! -f main.pdf || cp -v main.pdf versions/$(version)-draft.pdf
	@test ! -f main.tex || cp -v main.tex versions/$(version)-draft.tex
	@test ! -f letter.pdf || cp -v letter.pdf versions/$(version)-letter.pdf
	@test ! -f letter.tex || cp -v letter.tex versions/$(version)-letter.tex
	@test ! -f response.pdf || cp -v response.pdf versions/$(version)-response.pdf
	@test ! -f response.tex || cp -v response.tex versions/$(version)-response.tex
	@test ! -f diff.pdf || test ! -f "$(oldtex)" || cp -v diff.pdf versions/$(version)-$(oldhash)-diff.pdf
	@test ! -f diff.tex || test ! -f "$(oldtex)" || cp -v diff.tex versions/$(version)-$(oldhash)-diff.tex

submodules:
	# check out all submodules in this repository
	git submodule init
	git submodule update
	$(MAKE) -C filters/available submodules

clean:
	# remove reproducable files
	$(RM) main.tex main.pdf response.tex response.pdf letter.tex letter.pdf diff.*
	$(RM) -r '$(auxdir)'

.PHONY: all clean copy-to-versions diff letter main response submodules version
